$ = jQuery;
$(document).ready(function() {
   // toggle the component with class accordion_body
        $(".accordion_head").click(function() {
            if ($('.accordion_body').is(':visible')) {
            $(".accordion_body").slideUp(300);
            $(".plusminus").text('+');
            }
            if ($(this).next(".accordion_body").is(':visible')) {
            $(this).next(".accordion_body").slideUp(300);
            $(this).children(".plusminus").text('+');
            } else {
            $(this).next(".accordion_body").slideDown(300);
            $(this).children(".plusminus").text('-');
            }
        });


        //slick-project
        $('.project-list > ul').slick({
            slidesToShow: 3,
            speed: 300,
            infinite: false,
            slidesToScroll: 1,
            responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                  },
                },
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                  },
                },
              ],
          });
         

        //slick-review
        $('.review-list > ul').slick({
            slidesToShow: 2,
            speed: 800,
            infinite: false,
            slidesToScroll: 2,
            responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    adaptiveHeight: true,
                  },
                },
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                  },
                },
              ],
          });

        //pricing-list
        $('.pricing-bottom > ul').slick({
            slidesToShow: 3,
            speed: 800,
            infinite: false,
            slidesToScroll: 1,
            responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: true,
                  },
                },
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: true,
                  },
                },
              ],
          });



        //nav
        var hasSubMenu = jQuery('.nav-menu').children('li.nav-item.nav-item--expanded').children('ul').hasClass('nav-menu-sub')
        if(hasSubMenu) {
          jQuery('.nav-menu > li.nav-item.nav-item--expanded').append('<span class="arrow-icon"></span>')
        }


        $(function() {
          $(".arrow-icon").click(function() {
              $(".nav-menu-sub").slideToggle(300);
          });
    }); 



        // //active class nav
        // $('.nav-item').each(function () {
        //   if (this.href === path) {
        //     $(this).addClass('active');
        //   }
        // });
    });



//hamburger
var hamburger = document.querySelector(".hamburger");
var navMenu = document.querySelector(".nav-menu");

hamburger.addEventListener("click", mobileMenu);

function mobileMenu() {
    hamburger.classList.toggle("active");
    navMenu.classList.toggle("active");
}
var navLink = document.querySelectorAll(".nav-link");

navLink.forEach(n => n.addEventListener("click", closeMenu));

function closeMenu() {
    hamburger.classList.remove("active");
    navMenu.classList.remove("active");
}

