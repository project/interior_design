# Interior Design

Interior Design is a custom theme that provides many classes in its markup.
For cleaner markup (fewer classes), the default Stable custom theme
can be used instead.

To use Interior Design as your custom theme, set the 'custom theme'
in your theme's .info.yml file to "Interio_Design":
  custom theme: Interio_Design

See [documentation](https://www.drupal.org/docs/8/theming-drupal-8/using-Interio_Design-as-a-custom-theme)
for more information on using the Interio_Design theme.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/interior_design).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/interior_design).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

- [Classy](https://www.drupal.org/project/classy)


## Installation

Install as you would normally install a contributed Drupal theme. For further
information, see
[Installing Drupal Themes](https://www.drupal.org/docs/extending-drupal/installing-themes).


## Configuration

### About Template1 Section

You have to add two classes in Fields section in view of TEMPLATE1
In body field add about_template_1_body
In image field add about_template_1_image

Also in views advance option add CSS class template1 wrapper


### About Template2 Section

You have to add two classes in Fields section in view of TEMPLATE2
In body field add about_template_2_body
In image field add about_template_2_image

Also in views advance option add CSS class template2 wrapper


### About Template3 Section

You have to add two classes in Fields section in view of TEMPLATE3
In body field add about_template_3_body
In image field add about_template_3_image

Also in views advance option add CSS class template3 wrapper


### About Project Section

Add class in projects views image field project_image


### About Pricing Section

Add class pricing in pricing views body field


### About Question Section

Add class questions in questions views body field


### About Team Section

You have to add two classes in Fields section in view of TEAMS
In body field add team-info
In image field add team-image

Also in views advance option add CSS class team_area wrapper .


## Maintainers

- Divyanshi Agarwal - [Divyanshi](https://www.drupal.org/u/divyanshi)
